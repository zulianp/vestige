vestigeModule.controller("HomeController", ["$scope","$interval", "$http",
	function($scope,$interval,$http) 
	{
		var socket = new WebSocket("ws://localhost:8080/VestigeWeb/VestigeWebSocket");
		var socketOpen = false;
		var id = null;
		$scope.currentAction = "select";
		$scope.objects = [];

		
		document.getElementById('matricesList').style.height = getSize().h-200-10-65+"px";

		var reopenSocket = function()
		{

		};

		var sendIvoke = function(invoke)
		{
			if(!socketOpen || id == null) {
				reopenSocket();
				return;
			}
			
			socket.send(JSON.stringify(invoke));
		}

		var onMessageOpen = function(data) 
		{
			if(data.to.length>=1){
				id=data.to[0];
				console.log("i am "+ id);
			}
		};


		var onMessageSessions = function(data) 
		{
			console.log(data);
			$scope.sessions = data.data.sessions;
			$scope.$apply();
		};

		var onMessageData = function(data) 
		{
			var mat=data.data;
			mat.session = data.from;
			mat.created = "22-jan-2016";
			mat.nonZero = mat.rows*mat.cols; //TODO 

			$scope.objects.push(mat);
			$scope.$apply();
		};

		var funcs = {
			OPEN: onMessageOpen,
			DATA: onMessageData,
			SESSIONS: onMessageSessions
		};



		socket.onopen = function()
		{
			console.log("open...");
			socketOpen = true;
		};

		socket.onmessage = function (evt) 
		{ 
			var data = JSON.parse(evt.data);
			console.log("Message received..." + data.type);
			
			funcs[data.type](data);
		};

		socket.onclose = function()
		{ 
			console.log("Connection is closed..."); 
			socketOpen = false;
			id = null;
		};




		$scope.matrixClick = function(matrix)
		{
			$scope.currentMatrix = matrix;
		}


		$scope.continueSession = function(session)
		{
			var invoke = newInvoke("COMMAND",id);
			invoke.to = [session.name];
			invoke.data = {
				type: "command",
				commandType: "CONTINUE"
			};

			sendIvoke(invoke);
		};

		$scope.stopSession = function(session)
		{
			var invoke = newInvoke("COMMAND",id);
			invoke.to = [session.name];
			invoke.data = {
				type: "command",
				commandType: "STOP"
			};

			sendIvoke(invoke);
		};

		$scope.actionSelect = function()
		{
			$scope.currentAction = "select";
		}

		$scope.actionPan = function()
		{
			$scope.currentAction = "pan";
		}

		$scope.actionZoomIn = function()
		{
			$scope.currentAction = "zoomIn";
		}

		$scope.actionZoomOut = function()
		{
			$scope.currentAction = "zoomOut";
		}

		$scope.actionReset = function()
		{
			$scope.currentAction = "reset";
		}



		

		var sendPing = function()
		{
			sendIvoke(newInvoke("PING",id));
		}

		$interval(sendPing, 10000);





		$scope.loadScript = function()
		{
			document.getElementById('load-script').click();
		}

		$scope.currentScript="";
		$scope.scriptLoaded = function(scriptContent)
		{
			$scope.currentScript=scriptContent;
		}


		// var w=$('#mainCanvas').width()-250;
		// $('#mainCanvas').width(w);
		// $('#history').width(w);

		// var mustGetData=false;
		// $scope.historySelected=null;
		// var changedEntries=[];

		// var getConnected = function()
		// {
		// 	$http.get(baseURL()+"data/getConnected")
		// 	.success(function(data) {
		// 		if(data.error)
		// 			console.log(data.message);
		// 		else
		// 		{
		// 			$scope.connected=data;

		// 			if(!$scope.currentClient && data.length>0)
		// 				$scope.currentClient=data[data.length-1];
		// 		}
		// 	})
		// 	.error(function(error)
		// 	{
		// 		console.log(error);
		// 	});
		// }

		// var getHistory = function()
		// {
		// 	$http.get(baseURL()+"data/getHistory")
		// 	.success(function(data) {
		// 		if(data.error)
		// 			console.log(data.message);
		// 		else
		// 		{
		// 			if(!$scope.history || $scope.history.length != data.length)
		// 				mustGetData=true;
		// 			$scope.history=data;
		// 		}
		// 	})
		// 	.error(function(error)
		// 	{
		// 		console.log(error);
		// 	});
		// }

		// var getLastData = function()
		// {
		// 	if($scope.historySelected) return;

		// 	if(!$scope.currentClient) return;

		// 	if($scope.data && !$scope.currentClient.active) return;

		// 	if(!mustGetData) return;

		// 	$http.post(baseURL()+"data/getLast",$scope.currentClient.id)
		// 	.success(function(data) {
		// 		if(data.error)
		// 		{

		// 		}
		// 		else{
		// 			$scope.data=data;
		// 			mustGetData=false;
		// 		}
		// 	})
		// 	.error(function(error)
		// 	{
		// 		console.log(error);
		// 	});	
		// }

		// var getData = function()
		// {
		// 	$http.post(baseURL()+"data/get",$scope.historySelected)
		// 	.success(function(data) {
		// 		if(data.error)
		// 		{

		// 		}
		// 		else{
		// 			$scope.data=data;
		// 		}
		// 	})
		// 	.error(function(error)
		// 	{
		// 		console.log(error);
		// 	});	
		// }



		// getConnected();
		// getHistory();

		// // var refreshData = function() {
		// // 	getConnected();
		// // 	getHistory();
		// // 	getLastData();
		// // };

		// // $scope.connected=[];


		// // $interval(refreshData, 1000);

		// var sendCommand = function(clientid, command, data)
		// {
		// 	var obj={
		// 		clientID: clientid,
		// 		command: command
		// 	};

		// 	if(data)
		// 		obj.data=data;

		// 	$http.post(baseURL()+"data/command",obj);
		// }

		// $scope.select = function(client)
		// {
		// 	changedEntries=[];
		// 	$scope.data=null;
		// 	mustGetData=true;
		// 	$scope.historySelected=null;
		// 	$scope.currentClient=client;
		// }

		// $scope.disconnect = function(client)
		// {
		// 	if($scope.currentClient==client.id)
		// 		$scope.currentClient.active=false;

		// 	sendCommand(client.id,"exit");
		// }

		// $scope.continue = function(client)
		// {
		// 	sendCommand(client.id,"continue");
		// }
		
		// $scope.run = function(client, ast)
		// {
		// 	sendCommand(client.id, "run", ast);
		// };
		
		// $scope.save = function(client)
		// {
		// 	if(!changedEntries || changedEntries.length<=0) return;

		// 	var msg=[];

		// 	for (var i = 0; i < changedEntries.length; i++) {
		// 		var process=changedEntries[i].process;
		// 		if(!msg[process])
		// 		{
		// 			for (var j = msg.length; j <= process; ++j) {
		// 				msg.push({});
		// 			}
		// 		}

		// 		tmp=msg[process];

		// 		if(!tmp.rows) tmp.rows=[];
		// 		if(!tmp.cols) tmp.cols=[];
		// 		if(!tmp.entries) tmp.entries=[];

		// 		tmp.rows.push(changedEntries[i].row);
		// 		tmp.cols.push(changedEntries[i].col);
		// 		tmp.entries.push(parseFloat(changedEntries[i].value));
		// 	}


		// 	sendCommand(client.id,"edit", msg);
		// 	changedEntries=[];
		// }


		// $scope.matrixEntryChanged = function(entryData)
		// {
		// 	var index=-1;

		// 	for(var i=0;i<$scope.data.data.length;++i)
		// 	{
		// 		if($scope.data.data[i].process===entryData.process)
		// 		{
		// 			index=i;
		// 			break;
		// 		}
		// 	}

		// 	if(index<0) return;

		// 	$scope.data.data[index].entries[entryData.index]=entryData.value;

		// 	var found=false;
		// 	for(var i=0;i<changedEntries.length;++i)
		// 	{
		// 		if(changedEntries[i].col==entryData.col && changedEntries[i].row==entryData.row && changedEntries[i].process==entryData.process)
		// 		{
		// 			changedEntries[i].value=entryData.value;
		// 			found=true;
		// 			break;
		// 		}
		// 	}

		// 	if(!found)
		// 		changedEntries.push(entryData);
		// }

		// $scope.selectHistory = function(clientID, messageID)
		// {
		// 	$scope.historySelected={clientID: clientID, messageID: messageID};
		// 	getData();
		// }

		// var mode="";

		// var ismousedown=false;
		// var mouseInteractionEnabled=false;
		// var matrixSelection=$('#matrixSelection');
		// matrixSelection.hide();
		// var startX, startY;

		// $scope.zoomBtCliecked = function()
		// {
		// 	mouseInteractionEnabled=true;
		// 	mode="zoom";
		// }

		// $scope.moveBtCliecked = function()
		// {
		// 	mouseInteractionEnabled=true;
		// 	mode="move";
		// }

		// $scope.mousedownfunc = function($event)
		// {
		// 	if(!mouseInteractionEnabled) return;
		// 	ismousedown=true;
		// 	matrixSelection.show();
		// 	startX=$event.clientX;
		// 	startY=$event.clientY;

		// 	matrixSelection.css('left', startX);
		// 	matrixSelection.css('top', startY);


		// 	matrixSelection.css('width', 1);
		// 	matrixSelection.css('height', 1);
		// };

		// $scope.mousemovefunc = function($event)
		// {
		// 	if(!mouseInteractionEnabled) return;
		// 	if(!ismousedown) return;

		// 	matrixSelection.css('width', $event.clientX-startX);
		// 	matrixSelection.css('height', $event.clientY-startY);
		// }

		// $scope.mouseupfunc = function($event)
		// {
		// 	if(!mouseInteractionEnabled) return;

		// 	if(mode=="zoom")
		// 	{
		// 		var zoomRect={x: startX, y: startY, width: $event.clientX-startX, height: $event.clientY-startY};

		// 		if(zoomRect.width>1e-6 && zoomRect.height>1e-6)
		// 			$scope.zoomRect=zoomRect;
		// 	}

		// 	ismousedown=false;
		// 	matrixSelection.hide();
		// }

		// $scope.$on('$destroy', function(){
		// 	if (angular.isDefined(promise)) {
		// 		$interval.cancel(promise);
		// 		promise = undefined;
		// 	}
		// });

	}]);