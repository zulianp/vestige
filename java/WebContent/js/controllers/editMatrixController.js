vestigeModule.controller("EditMatrixController", ["$scope", "$uibModalInstance", "entry", "matrix",
    function($scope,$uibModalInstance,entry,matrix)
    {
        $scope.entry=entry;
        $scope.matrix=matrix;

        var index=entry.i*matrix.cols+entry.j;

        $scope.value=matrix.entries[index];

        $scope.save = function()
        {
            if($scope.newValue==$scope.val)
                return;

            console.log("must send "+ $scope.newValue);
            $uibModalInstance.close();
        }
    }])

.directive('onlyDigits', function () {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attr, ctrl) {
        function inputValue(val) {
          if (val) {
            var digits = val.replace(/[^0-9.]/g, '');

            if (digits.split('.').length > 2) {
              digits = digits.substring(0, digits.length - 1);
            }

            if (digits !== val) {
              ctrl.$setViewValue(digits);
              ctrl.$render();
            }
            return parseFloat(digits);
          }
          return undefined;
        }            
        ctrl.$parsers.push(inputValue);
      }
    };
 });
