vestigeModule.directive('scriptingui', function () {
    function link(scope, element, attrs) {
    	scope.code = "";
    	scope.makeJSONAST = function() {
    		scope.ast = JSON.stringify(esprima.parse(scope.code),null, Number(4));   
    		return esprima.parse(scope.code)
    	};
    	
    	scope.clearCode = function() {
    		scope.code = "";
    	};
    }

    return {
        templateUrl: 'templates/scriptingui.html',
        link: link
    };
});