vestigeModule.directive('matrix', ["$uibModal", function ($uibModal) {
    function link(scope, element, attrs) {
        var image=null;
        var currentData = null;
        var side=-1;
        var currentAction="select"

        var zoomLevel=1;
        var minZoom =1;
        var tx=0, ty=0;
        var startTX=0, startTY=0;

        scope.$watch(attrs.data, function(newData,oldData,scope) {
            renderMatrix(newData,oldData,scope);
        }, true);

        scope.$watch(attrs.action, function(newData,oldData,scope) {
            if(!image) return;

            if(newData=="reset")
            {
                zoomLevel = 1;
                tx=startTX;
                ty=startTY;
                rerender();
                return;
            }
            currentAction=newData;

            if(currentAction=="select")
                scope.cursorType="cursorPointer";
            else if(currentAction=="pan")
                scope.cursorType="cursorMove";
            else if(currentAction=="zoomIn")
                scope.cursorType="cursorZoomin";
            else if(currentAction=="zoomOut")
                scope.cursorType="cursorZoomout";

        }, true);



        var renderMatrix = function(data,oldData,scope) {
            if(!data) return;
            currentData=data;

            currentAction="select"
            zoomLevel=1;
            tx=0; ty=0;


            var tmp = element[0];
            var container = tmp.children[0];
            var canvas = container.children[0];


            var s = getSize(); 
            var w = s.w-250-10;
            var h = s.h-200-35;

            // container.style="width: "+w+"px; height: "+h+"px";
            // container.height=h;

            var sx=w/data.cols;
            var sy=h/data.rows;

            var spacing = 5;
            side=Math.min(sx,sy);
            if(side<spacing) spacing=side*0.1;
            // side=Math.max(sidetmp,2+spacing);
            // minZoom=sidetmp/side;
            zoomLevel=minZoom;

            canvas.width=w;
            canvas.height=h;

            

            var dataMin=dataMax=data.entries[0];
            

            for(var i=1;i<data.entries.length;++i)
            {
                dataMin=Math.min(dataMin,data.entries[i]);
                dataMax=Math.max(dataMax,data.entries[i]);
            }

            var cw = side*data.cols;
            
            
            tx=(w-cw)/2;
            startTX=tx;
            startTY=0;


            var ctx = canvas.getContext('2d');
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            // ctx.setTransform(zoomLevel,0,0,zoomLevel,tx,ty);

            for(var i=0;i<data.rows;++i)
            {
                for(var j=0;j<data.cols;++j)
                {
                    var index=i*data.cols+j;

                    var val=(data.entries[index]-dataMin)/(dataMax-dataMin);
                    var col=computeColor(val);
                    
                    ctx.fillStyle = 'rgb('+Math.floor(col.r*255)+','+Math.floor(col.g*255)+','+Math.floor(col.b*255)+')';
                    ctx.fillRect(j*side+spacing, i*side+spacing, side-spacing, side-spacing);
                }
            }

            image = new Image();
            image.src = canvas.toDataURL('image/png');
            rerender();
        };

        var prevX=NaN, prevY=NaN;

        scope.matrixClicked = function(evt)
        {
            if(!currentData) return;

            var mx=evt.offsetX;
            var my=evt.offsetY;

            if(currentAction=="select")
            {
                var x = Math.floor((mx-tx)/zoomLevel/side);
                var y = Math.floor((my-ty)/zoomLevel/side);

                if(x<0 || x>currentData.cols) return;
                if(y<0 || y>currentData.rows) return;

                var modalInstance = $uibModal.open({
                    animation: false,
                    templateUrl: 'templates/editMatrix.html',
                    controller: 'EditMatrixController',
                    resolve: {
                        entry: function () { return {i: y, j: x}; },
                        matrix: function () { return currentData; }
                    }
                });
            }
            else if(currentAction=="zoomIn")
            {
                tx+=mx*zoomLevel;
                ty+=my*zoomLevel;

                zoomLevel+=0.1;

                tx-=mx*zoomLevel;
                ty-=my*zoomLevel;

                rerender();
            }
            else if(currentAction=="zoomOut")
            {
                tx+=mx*zoomLevel;
                ty+=my*zoomLevel;

                zoomLevel-=0.1;
                zoomLevel=Math.max(minZoom,zoomLevel);

                tx-=mx*zoomLevel;
                ty-=my*zoomLevel;

                rerender();
            }
            else if(currentAction=="pan")
            {
                prevX=NaN;
                prevY=NaN;
            }
        }


        scope.matrixMouseMove = function(evt)
        {
            if(!currentData) return;
            if(currentAction!="pan") return;
            if(!evt.buttons) return;

            var mx=evt.offsetX;
            var my=evt.offsetY;

            if(!isNaN(prevX) && !isNaN(prevY)){
                tx+=mx-prevX;
                ty+=my-prevY;
            }

            prevX=mx;
            prevY=my;

            rerender();
        }

        var rerender = function()
        {
            if(!image) return;

            var tmp = element[0];
            var container = tmp.children[0];
            var canvas = container.children[0];

            var ctx = canvas.getContext('2d');
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            ctx.setTransform(zoomLevel,0,0,zoomLevel,tx,ty);

            ctx.drawImage(image, 0, 0);
        }

    }

    return {
        templateUrl: 'templates/matrix.html',
        link: link
    };
}]);
