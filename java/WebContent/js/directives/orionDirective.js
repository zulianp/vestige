vestigeModule.directive('orion', [ '$interval', function ($interval) {
  var link = function(scope, templateElement, attrs) {
    var editor;
    scope.codeValid=false;
    var syntax;
    scope.errors = [];

    var validate = function(){
      try {
        editor.removeAllErrorMarkers();
        scope.errors = [];

        var code=editor.getText();

        syntax = esprima.parse(code, { tolerant: true, loc: true });
        var errors = syntax.errors;

        if (errors.length > 0) {
          for (var i = 0; i < errors.length; ++i){
            editor.addErrorMarker(errors[i].index, errors[i].description);
            scope.errors.push({type: 'danger', msg: errors[i].message + ":"+errors[i].column});
          }
        } else {
          scope.errors.push({type: 'success', msg: "Code is syntactically valid"});

          scope.codeValid = code.length>0;
        }
      } 
      catch (e) {
        editor.addErrorMarker(e.index, e.description);

        scope.errors.push({type: 'danger', msg: e.toString()});
      }
    }

    scope.buildScript = function()
    {
      if(scope.codeValid)
      {
        console.log(syntax);
      }
    }


    require(["custom/editor"], function(edit) {
      editor = edit({parent: "scripteditor", lang: 'js'});
      editor.getTextView().getModel().addEventListener("Changed", function () { validate(); });
      editor.setText("");

      $interval(validate,500);
    });

    scope.$watch(attrs.data, function(newData,oldData,scope) {
      if(editor)
      {
        editor.setText(newData);
        validate();
      }
    });

  };

  var orion = {
    templateUrl: 'templates/scriptEditor.html',
    replace: true,
    link: link
  };
  return orion;
}]);
