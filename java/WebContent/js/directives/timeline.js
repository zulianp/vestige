vestigeModule.directive('timeline', function () {
    function link(scope, element, attrs) {

        scope.$watchGroup([attrs.data, attrs.selected], function(newData, oldData, scope) {
            renderTimeline(newData, oldData, scope);
        }, true);

        var renderTimeline = function(data,oldData,scope) {
            var time=data[0];
            var oldTime=oldData[0];
            var current=data[1];
            var oldCurrent=oldData[1];

            // if(oldTime && data.length===oldData.length && oldCurrent && current.id===oldCurrent.id) return;

            var svgCont = element[0];
            
            for (var i = 0; i < svgCont.children.length; ++i) 
                svgCont.children[i].remove();

            if(!time) return;
            if(!current) return;

            
            var h=$(svgCont.parentElement).height()-20;


            
            var rectWidth=30;
            var offset=10;
            var offsetH=10;

            var svg = d3.select(svgCont).append("svg")
            .attr("width", (rectWidth+offset)*time.length)
            .attr("height", h);

            var maxSize=0;
            var maxLength=1;

            var selectedIndex=time.length-1;

            for(var i=0;i<time.length;++i)
            {
                var locSize=0;
                var locLength=time[i].sizes.length;
                time[i].plotData=new Array(locLength);

                for(var j=0; j<locLength;++j){
                    time[i].plotData[j]={};
                    time[i].plotData[j].start=locSize;
                    time[i].plotData[j].clientID=time[i].clientID;

                    locSize+=time[i].sizes[j];
                    
                    time[i].plotData[j].end=locSize;
                }

                time[i].locSize=locSize;

                maxSize=Math.max(maxSize,locSize);
                maxLength=Math.max(maxLength,locLength);

                var selected=scope.historySelected!=null && time[i].clientID===scope.historySelected.clientID && time[i].messageID===scope.historySelected.messageID;
                if(selected)
                    selectedIndex=i;
            }

            var scale = d3.scale.linear()
            .domain([0, maxSize])
            .range([0, h-offsetH]);


            var color = colorScale(0, maxLength);
            var gray = grayColorScale(0,maxLength);


            var cols = svg.selectAll("g.col")
            .data(time)
            .enter()
            .append("g")
            .attr("transform", function(d,i) { return "translate(" + (rectWidth+offset)*i + ",0)"; })
            .attr("class", "cursorPointer")
            .on("click",function(d){ scope.selectHistory( d.clientID,d.messageID); });



            cols.selectAll("rect")
            .data(function(d) { return d.plotData; })
            .enter()
            .append("rect")
            .attr("width", rectWidth)
            .attr("y", function(d) { return h-scale(d.end)-scale(d.start)-offsetH; })
            .attr("height", function(d) { return scale(d.end); })
            .style("fill", function(d,i) { return d.clientID===current.id?color(i):gray(i); });

            svg
            .append("circle")
            .attr("cx", (rectWidth+offset)*selectedIndex+rectWidth/2-1.5)
            .attr("cy", h-3)
            .attr("r",3)
            .attr("class","grayFill");
        };

    }

    return {link: link};
});