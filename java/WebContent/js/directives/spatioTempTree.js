vestigeModule.directive('spatioTemporalTree', function () {
    function link(scope, element, attrs) {
        scope.$watch(attrs.data, function(newData,oldData,scope) {
            renderTree(newData,oldData,scope);
        }, true);

        var renderTree = function(data,oldData,scope) {
            var container = element[0];

            if(!data) return;

            var containsKey = function(nodes,key)
            {
                for(var j=0;j<nodes.length;++j)
                {
                    if(nodes[j].title===key)
                        return j;
                }

                return -1;
            }

            var root = {
                title: "root",
                nodes: []
            };

            for (var i = 0; i < data.length; i++) {
                var path=data[i].name.split('/');

                var currentNode=root;
                for(var j=0;j<path.length;++j)
                {
                    var index=containsKey(currentNode.nodes,path[j]);
                    if(index>=0 && j!=path.length-1)
                    {
                        currentNode=currentNode.nodes[index];
                        continue;
                    }

                    var tmp={ title: path[j], nodes: []};
                    currentNode.nodes.push(tmp);
                    currentNode=tmp;
                }

            }

            scope.treeData=[root]
        };

    }

    return {
        templateUrl: 'templates/treeView.html',
        link: link
    };
});