var vestigeModule = angular.module( "vestigeModule", ["flow", "ngRoute", "pascalprecht.translate", "ui.bootstrap", "ui.tree", "hljs"]);

vestigeModule.config(["$routeProvider", "hljsServiceProvider", 
  function($routeProvider, hljsServiceProvider) {   
    $routeProvider.otherwise(
    {
        templateUrl: "templates/home.html",
        controller: "HomeController"
    });
    
    hljsServiceProvider.setOptions({
	    // replace tab with 4 spaces
	    tabReplace: '    '
   });
}]);


var newInvoke = function(type, from)
{
    return  {
        type: type,
        data: {
            type: "void"
        },
        from: from,
        to: [],
        context: "",
        node: ""
    };
}

var computeColor = function(val)
{
    var res;
    if(val < 0.2) {
        var t = val*5;
        res = {
            r: (1-t) * 0.46 + t * 0.83, 
            g: 0,
            b: 0
        };
    }
    else if(val < 0.5) {
        var t = (val-0.2)/0.3;
        res =  {
            r: (1-t)*0.83+t,
            g: t*0.64,
            b: 0
        }
    }
    
    else if(val < 0.75) {
        var t = (val-0.5)*4;
        res =  {
            r: 1,
            g: (1-t)*0.64+t,
            b: 0
        };
    }
    else{
        var t = (val-0.75)*4;
        res = {
            r: 1,
            g: 1,
            b: t
        };
    }

    return res;
}

var getSize = function() 
{
    if (self.innerHeight) {
        return {w: self.innerWidth, h: self.innerHeight};
    }

    if (document.documentElement && document.documentElement.clientWidth) {
        return {w: document.documentElement.clientWidth, h: document.documentElement.clientHeight};
    }

    if (document.body) {
        return {w: document.body.clientWidth, h: document.body.clientHeight};
    }
}






