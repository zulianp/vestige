package vestige.server.web;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import vestige.program.ProgramSocket;


//@ApplicationPath("/Vestige")
public class VestigeWebServlet extends HttpServlet
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public class SocketServerRunner implements Runnable
	{
		@Override
		public void run()
		{
			final ExecutorService executor = Executors.newCachedThreadPool();
			ServerSocket listener = null;
			try {
				listener = new ServerSocket(8888);
				while (true)
					executor.execute(new ProgramSocket(listener.accept()));
			} catch (final IOException e) {
				e.printStackTrace();
			}finally {
				if(listener!=null){
					try {
						listener.close();
					} catch (final IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	public void init() throws ServletException
	{     
		new Thread(new SocketServerRunner()).start();
	}


}
