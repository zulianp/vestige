package vestige.vis.json;

import java.io.IOException;
import java.io.StringWriter;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import vestige.object.Invoke;

public class InvokeEncoderDecoder implements Encoder.Text<Invoke>,  Decoder.Text<Invoke> {
	private ObjectMapper mapper;
	
	
	@Override
	public void destroy() {
		
	}

	@Override
	public void init(EndpointConfig arg0) {
		mapper = new ObjectMapper();
	}

	@Override
	public String encode(Invoke value) throws EncodeException {
		StringWriter out = new StringWriter();
		try {
			mapper.writeValue(out, value);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
			throw new EncodeException(value,e.getMessage());
		} catch (JsonMappingException e) {
			e.printStackTrace();
			throw new EncodeException(value,e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			throw new EncodeException(value,e.getMessage());
		}
		
		return out.toString();
	}

	@Override
	public Invoke decode(String value) throws DecodeException {
		try {
			return mapper.readValue(value, Invoke.class);
		} catch (IOException e) {
			e.printStackTrace();
			throw new DecodeException(value,e.getMessage());
		}
	}

	@Override
	public boolean willDecode(String arg0) {
		return true;
	}

}
