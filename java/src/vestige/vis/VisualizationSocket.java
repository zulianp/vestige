package vestige.vis;

import java.io.IOException;

import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import vestige.object.Invoke;
import vestige.object.Invoke.Type;
import vestige.service.MessageBus;
import vestige.service.MessageListener;
import vestige.vis.json.InvokeEncoderDecoder;


@ServerEndpoint(value = "/VestigeWebSocket",  encoders = {InvokeEncoderDecoder.class}, decoders = {InvokeEncoderDecoder.class})
public class VisualizationSocket implements MessageListener
{
	private String id;
	private final MessageBus messageBus;
	private Session session;
	//	private ObjectMapper mapper;


	@OnOpen
	public void onOpen(final Session session){
		this.session= session;
		this.id = session.getId();
		messageBus.addVisualization(id, this);

		try {
			session.getBasicRemote().sendObject(Invoke.NewOpenWebSocket(this.id));
			session.getBasicRemote().sendObject(messageBus.getSessions());
			for(final Invoke i : messageBus.getData())
				session.getBasicRemote().sendObject(i);
		} catch (final Exception ex) {
			ex.printStackTrace();
		}
	}

	@OnMessage
	public void onMessage(final Invoke message, final Session session)  throws IOException, EncodeException{
		if(message.getType()!=Type.PING)
			messageBus.newVisualizationMessage(message);
	}

	@OnClose
	public void onClose(final Session session){
		assert(session.getId().equals(id));
		messageBus.removeVisualization(id);
	}


	public VisualizationSocket() 
	{
		session = null;
		messageBus = MessageBus.Instance();
		//		mapper = new ObjectMapper();
	}

	@Override
	public synchronized void sendMessage(final Invoke invoke) {
		if(session == null) return;
		try {
			session.getBasicRemote().sendObject(invoke);
		} catch (IOException | EncodeException e) {
			e.printStackTrace();
		}
	}
}
