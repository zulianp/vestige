package vestige.old;

public class Session {
	private String sessionID;
	private int connected;

	public Session()
	{
		sessionID="";
		connected=0;
	}

	public Session(final String sessionID)
	{
		this.sessionID=sessionID.trim();
		connected=1;
	}

	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(final String sessionID) {
		this.sessionID = sessionID;
	}
	public int getConnected() {
		return connected;
	}
	public void setConnected(final int connected) {
		this.connected = connected;
	}

	@Override
	public int hashCode()
	{
		return sessionID.hashCode();
	}

	@Override
	public boolean equals(final Object o)
	{
		if(o==null) return false;
		if(o instanceof Session)
			return sessionID.equals(((Session)o).getSessionID());
		
		return sessionID.equals(o);
	}

	public void addConnection() {
		++connected;
	}
	
	public String toString()
	{
		return sessionID+"->"+connected;
	}


}
