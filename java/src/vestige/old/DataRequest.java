package vestige.old;

public class DataRequest {
	private String clientID;
	private String messageID;

	public DataRequest()
	{
		clientID="";
		messageID="";
	}

	public String getClientID() {
		return clientID;
	}
	public void setClientID(final String clientID) {
		this.clientID = clientID;
	}
	public String getMessageID() {
		return messageID;
	}
	public void setMessageID(final String messageID) {
		this.messageID = messageID;
	}


}
