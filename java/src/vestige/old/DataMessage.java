package vestige.old;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import vestige.object.data.Matrix;

public class DataMessage {
	private int id;
	private List<Matrix> data;
	private String clientID;
	private String messageID;
	private String name;
	
	public DataMessage()
	{
		this.id = -1;
		this.data = new ArrayList<>();
		this.clientID="";
		this.messageID = UUID.randomUUID().toString();
	}
	
	
	public DataMessage(IncomingMessage msg) {
		this();
		this.clientID=msg.getClientID();
		this.id=msg.getId();
		this.name=msg.getName();
		data.add(msg);
	}


	public int getId() {
		return id;
	}
	public List<Matrix> getData() {
		return data;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setData(List<Matrix> data) {
		this.data = data;
	}

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public String getMessageID() {
		return this.messageID;
	}
	
	public void setMessageID(String id) {
		this.messageID = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
}
