package vestige.old;

public class SocketClient {
	private String id;
	private boolean active;
	
	public SocketClient(final String id, final boolean active) {
		this.id = id;
		this.active = active;
	}
	
	public String getId() {
		return id;
	}
	public void setId(final String id) {
		this.id = id;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(final boolean active) {
		this.active = active;
	}
	
	public String toString()
	{
		return id+(active?"active":"inactive");
	}
	
	
}
