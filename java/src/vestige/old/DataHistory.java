package vestige.old;

import java.util.ArrayList;
import java.util.List;

public class DataHistory {
	private String clientID;
	private String messageID;
	private String name;
	private List<Integer> sizes;
	private final int id;

	public DataHistory()
	{
		sizes=new ArrayList<>();
		this.clientID="";
		this.messageID="";
		this.name="";
		this.id=-1;
	}

	public DataHistory(final DataMessage msg)
	{
		sizes=new ArrayList<>();
		this.clientID=msg.getClientID();
		this.messageID=msg.getMessageID();
		this.name=msg.getName();
		this.id=msg.getId();
	}

	public void addSize(final int proc, final int size)
	{
		while(sizes.size()<=proc)
			sizes.add(0);

		this.sizes.set(proc, size);
	}

	public String getClientID() {
		return clientID;
	}

	public List<Integer> getSizes() {
		return sizes;
	}

	public void setClientID(final String clientID) {
		this.clientID = clientID;
	}

	public void setSizes(final List<Integer> sizes) {
		this.sizes = sizes;
	}

	public int getID() {
		return id;
	}

	public String getMessageID() {
		return messageID;
	}

	public void setMessageID(final String messageID) {
		this.messageID = messageID;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}
}
