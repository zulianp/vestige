package vestige.old;

import vestige.object.data.Matrix;

public class IncomingMessage extends Matrix{
	private String clientID;
	private String name;
	private int id;
	
	public IncomingMessage()
	{
		clientID="";
		name="name";
		id=-1;
	}

	public String getClientID() {
		return clientID;
	}

	public void setClientID(final String clientID) {
		this.clientID = clientID;
	}

	public int getId() {
		return id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}
}
