package vestige.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.Socket;
import java.util.Random;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import vestige.object.Invoke;
import vestige.object.data.CommandData;
import vestige.object.data.DenseMatrix;


public class SendDataExample {

	private static ObjectMapper mapper;
	private static String sessionId = "sessionId";

	public static void main(final String[] args) throws IOException {
		final String hostName = "localhost";
		final int portNumber = 8888;

		final Socket socket = new Socket(hostName, portNumber);
		final PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
		final BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

		mapper = new ObjectMapper();

		final Invoke inv = Invoke.NewOpen(sessionId);
		send(out,inv);

		boolean exit=false;
		while(true){ //simulate the program
			boolean cont=false;
			
			createRandomMatrix(rand.nextInt(30)+1,rand.nextInt(30)+1,out);
//			createRandomMatrix(500,700,out);

			while(true)
			{
				final StringBuilder sbuilder = new StringBuilder();
				String inputLine=in.readLine();

				if(inputLine==null || inputLine.length() == 0) continue;

				final int size = Integer.parseInt(inputLine);

				while((inputLine = in.readLine()) != null) {
					sbuilder.append(inputLine + "\n");	
					if(sbuilder.length() >= size) {
						break;
					}
				}

				final String content = sbuilder.toString();

				if(content==null || content.length()<=0) {		
					continue;
				}

				if(!socket.isConnected() || socket.isClosed())
					break;

				System.out.println(content);
				final Invoke invoke = mapper.readValue(content, Invoke.class);

				switch(invoke.getType())
				{
				case PING: break;
				case COMMAND:
				{
					CommandData data= (CommandData) invoke.getData();

					switch (data.getCommandType()) {
					case STOP:
						exit=true;
						break;
					case CONTINUE:
						cont=true;
						break;
					default:
						break;
					}

				}
				default:
					break;
				}

				if(exit || cont) break;
			}

			if(exit) break;
		}

		socket.close();
	}

	static void send(final PrintWriter out, final Invoke invoke) throws JsonGenerationException, JsonMappingException, IOException
	{
		final StringWriter buf=new StringWriter();
		mapper.writeValue(buf, invoke);
		final String msg=buf.toString();

		out.println(msg.length()+"\n"+msg);
	}

	private static Random rand=new Random();

	public static void createRandomMatrix(final int rows, final int cols, final PrintWriter out) throws JsonGenerationException, JsonMappingException, IOException
	{
		DenseMatrix mat = new DenseMatrix();
		mat.setRows(rows);
		mat.setCols(cols);
		mat.setName("mat_"+rand.nextInt(10));

		for(int i=0;i<rows*cols;++i)
			mat.getEntries().add(rand.nextDouble());
		
		Invoke invoke=new Invoke();
		invoke.setType(Invoke.Type.DATA);
		invoke.setFrom(sessionId);
		invoke.setData(mat);
		
		send(out,invoke);
	}
}
