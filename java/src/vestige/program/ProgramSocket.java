package vestige.program;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;

import com.fasterxml.jackson.databind.ObjectMapper;

import vestige.object.Invoke;
import vestige.service.MessageBus;
import vestige.service.MessageListener;

public class ProgramSocket implements Runnable, MessageListener {
	private final Socket socket;

	private PrintWriter out;
	private BufferedReader in;

	private final ObjectMapper mapper;

	private final Timer timer;
	private String id;

	private final MessageBus messageBus;

	private boolean stop;

	public ProgramSocket(final Socket socket)
	{
		messageBus = MessageBus.Instance();
		mapper = new ObjectMapper();
		
		this.socket=socket;	
		in=null;
		out=null;
		stop=false;


		//Ping message
		timer=new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				if(out == null)
					disconnect();
				else{
					synchronized (ProgramSocket.this) {
						sendMessage(Invoke.NewPING());

						if (out.checkError()) 
							disconnect();
					}
				}

			}
		}, 5*1000, 5*1000);
	}


	@Override
	public void run()
	{
		try {    

			out = new PrintWriter(socket.getOutputStream(), true);                   
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			while(true){
				final StringBuilder sbuilder = new StringBuilder();
				if(stop) break;
				
				String inputLine = in.readLine();


				//				System.out.println(inputLine);


				if(inputLine==null || inputLine.length() == 0) continue;

				final int size = Integer.parseInt(inputLine);

				if(stop) break;
				while((inputLine = in.readLine()) != null) {
					sbuilder.append(inputLine + "\n");	
					if(sbuilder.length() >= size) {
						break;
					}
				}

				final String content = sbuilder.toString();

				if(content==null || content.length()<=0) {		
					continue;
				}

				if(!socket.isConnected() || socket.isClosed())
					break;

				System.out.println(content);
				final Invoke invoke = mapper.readValue(content, Invoke.class);

				if(invoke.getType()==Invoke.Type.OPEN)
				{
					id = invoke.getFrom();
					messageBus.addProgram(id, this);
				}
				else
				{
					messageBus.newProgramMessage(invoke);
				}
			}

			
		} catch (final IOException e) {
			e.printStackTrace();
		}

		disconnect();
	}


	private synchronized void disconnect() {
		stop=true;
		try {
			out.close();
			in.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		MessageBus.Instance().removeProgram(id);
		timer.cancel();
	}


	@Override
	public synchronized void sendMessage(final Invoke invoke) {
		if(out == null) return;
		
		try {
			final StringWriter sb = new StringWriter();
			mapper.writeValue(sb, invoke);

			final String str = sb.toString();
			out.println(str.length() + "\n" + str);
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

}
