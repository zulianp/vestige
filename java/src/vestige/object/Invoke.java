package vestige.object;

import java.util.ArrayList;
import java.util.List;

public class Invoke {
	public enum Type
	{
		NULL("NULL"),
		OPEN("OPEN"),
		PING("PING"),
		DATA("DATA"),
		PROGRAM("PROGRAM"),
		REQUEST("REQUEST"),
		SESSIONS("SESSIONS"),
		COMMAND("COMMAND")
		;


		private String name;

		Type(final String name)
		{
			this.name = name; 
		}

		public String getName() {
			return name;
		}
	}

	private Type type;
	private Data data;

	private String from;
	private List<String> to;

	private String context;
	private String node;


	public Invoke()
	{
		type = Type.NULL;
		data = new Data(); 

		from = "";
		to=new ArrayList<>();

		context = "";
		node= "";
	}


	public Type getType() {
		return type;
	}


	public Data getData() {
		return data;
	}


	public String getFrom() {
		return from;
	}


	public List<String> getTo() {
		return to;
	}


	public String getContext() {
		return context;
	}


	public String getNode() {
		return node;
	}


	public void setType(final Type type) {
		this.type = type;
	}


	public void setData(final Data data) {
		this.data = data;
	}


	public void setFrom(final String from) {
		this.from = from;
	}


	public void setTo(final List<String> to) {
		this.to = to;
	}


	public void setContext(final String context) {
		this.context = context;
	}


	public void setNode(final String node) {
		this.node = node;
	}


	public static Invoke NewPING() {
		final Invoke res = new Invoke();
		res.setType(Type.PING);
		return res;
	}

	public static Invoke NewOpenWebSocket(final String sessionId) {
		final Invoke res = new Invoke();
		res.setType(Type.OPEN);
		res.getTo().add(sessionId);
		return res;
	}

	public static Invoke NewOpen(final String sessionId) {
		final Invoke res = new Invoke();
		res.setType(Type.OPEN);
		res.setFrom(sessionId);
		return res;
	}

}
