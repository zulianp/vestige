package vestige.object;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

import vestige.object.data.CommandData;
import vestige.object.data.DenseMatrix;
import vestige.object.data.SessionData;


@JsonTypeInfo(  
		use = JsonTypeInfo.Id.NAME,  
		include = JsonTypeInfo.As.PROPERTY,  
		property = "type")  
@JsonSubTypes({  
	@Type(value = Data.class, 			name = "void"),
	@Type(value = DenseMatrix.class, 	name = "denseMatrix" ),
	@Type(value = SessionData.class, 	name = "sessions" ),
	@Type(value = CommandData.class, 	name = "command" ),
}) 
public class Data {
}
