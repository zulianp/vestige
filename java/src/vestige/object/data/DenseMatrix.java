package vestige.object.data;

import java.util.ArrayList;
import java.util.List;

import vestige.object.Data;

public class DenseMatrix extends Data
{
	private int process;
	private int nProcesses;
	

	private int rows, cols;
	private String name;
	
	private int offsetRow, offsetCol;
	private int localRows, localCols;
	private List<Double> entries;
	
	public DenseMatrix() {
		this.rows = 0;
		this.cols = 0;
		this.offsetRow = 0;
		this.offsetCol = 0;
		this.localRows = 0;
		this.localCols = 0;
		this.entries = new ArrayList<>();
		this.process = 0;
		this.nProcesses = 1;
	}

	public int getRows() {
		return rows;
	}
	public int getCols() {
		return cols;
	}
	public int getOffsetRow() {
		return offsetRow;
	}
	public int getOffsetCol() {
		return offsetCol;
	}
	public int getLocalRows() {
		return localRows;
	}
	public int getLocalCols() {
		return localCols;
	}
	public List<Double> getEntries() {
		return entries;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public void setCols(int cols) {
		this.cols = cols;
	}
	public void setOffsetRow(int offsetRow) {
		this.offsetRow = offsetRow;
	}
	public void setOffsetCol(int offsetCol) {
		this.offsetCol = offsetCol;
	}
	public void setLocalRows(int localRows) {
		this.localRows = localRows;
	}
	public void setLocalCols(int localCols) {
		this.localCols = localCols;
	}
	public void setEntries(List<Double> entries) {
		this.entries = entries;
	}

	public int size() {
		return localCols*localRows;
	}

	public int getProcess() {
		return process;
	}

	public void setProcess(int processor) {
		this.process = processor;
	}

	public int getnProcesses() {
		return nProcesses;
	}

	public void setnProcesses(int nProcesses) {
		this.nProcesses = nProcesses;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	
}
