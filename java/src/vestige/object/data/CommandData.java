package vestige.object.data;

import vestige.object.Data;

public class CommandData extends Data{
	public enum Type
	{
		CONTINUE("CONTINUE"),
		STOP("STOP")
		;
		
		
		private String name;
		
		Type(String name)
		{
			this.name = name; 
		}
		
		public String getName() {
			return name;
		}
	}
	
	private Type commandType;

	public Type getCommandType() {
		return commandType;
	}

	public void setCommandType(Type commandType) {
		this.commandType = commandType;
	}

}
