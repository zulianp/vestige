package vestige.object.data;

import java.util.ArrayList;
import java.util.List;

import vestige.object.Data;

public class SessionData extends Data {
	public class Session
	{
		private String name;
		private boolean active;
		
		public Session()
		{
			name="";
			active=false;
		}
		
		public Session(String name, boolean active)
		{
			this.name=name;
			this.active=active;
		}
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public boolean isActive() {
			return active;
		}
		public void setActive(boolean active) {
			this.active = active;
		}
	}
	
	private List<Session> sessions;
	
	public SessionData()
	{
		sessions=new ArrayList<>();
	}
	
	public List<Session> getSessions() {
		return sessions;
	}


	public void setSessions(List<Session> sessions) {
		this.sessions = sessions;
	}
	
	
}
