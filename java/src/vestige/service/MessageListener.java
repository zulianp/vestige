package vestige.service;

import vestige.object.Invoke;

public interface MessageListener {
	public void sendMessage(final Invoke invoke);
	
}
