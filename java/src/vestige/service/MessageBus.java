package vestige.service;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


import vestige.object.Invoke;
import vestige.object.Invoke.Type;
import vestige.object.data.SessionData;

public class MessageBus {
	private final Map<String, MessageListener> programs;
	private final Map<String, MessageListener> visualizations;

	private final List<String> oldPrograms;

	private static MessageBus instance = null;

	private List<Invoke> data;

	public synchronized static MessageBus Instance()
	{
		if(instance==null) instance=new MessageBus();

		return instance;
	}

	private MessageBus()
	{
		programs = new HashMap<>();
		oldPrograms = new ArrayList<>();
		data = new ArrayList<>();

		visualizations= new HashMap<>();
	}

	public synchronized void addProgram(final String programId, final MessageListener program)
	{
		if(programId == null) return;

		System.out.println("added program: " + programId);
		programs.put(programId, program);

		final Invoke sessions=getSessions();
		for(final Entry<String, MessageListener> e : visualizations.entrySet())
		{
			e.getValue().sendMessage(sessions);
		}
	}

	public synchronized void removeProgram(final String programId)
	{
		if(programId == null) return;

		if(programs.remove(programId)!=null){
			oldPrograms.add(programId);
			System.out.println("removed program: " + programId);


			final Invoke sessions=getSessions();
			for(final Entry<String, MessageListener> e : visualizations.entrySet())
			{
				e.getValue().sendMessage(sessions);
			}
		}
	}

	public synchronized void addVisualization(final String visId, final MessageListener visualization)
	{
		if(visId == null) return;

		System.out.println("added visualization: " + visId);
		visualizations.put(visId, visualization);
	}

	public synchronized void removeVisualization(final String visId)
	{
		if(visId == null) return;


		if(visualizations.remove(visId)!=null)
		{
			System.out.println("removed visualization: " + visId);
		}
	}

	public synchronized Invoke getSessions()
	{
		final Invoke res=new Invoke();
		res.setType(Type.SESSIONS);

		final SessionData data=new SessionData();

		for(final String s : programs.keySet())
			data.getSessions().add(data.new Session(s,true));

		for(final String s : oldPrograms)
			data.getSessions().add(data.new Session(s,false));

		res.setData(data);

		return res;
	}

	private synchronized void sendToPrograms(final Invoke message)
	{
		for(final String destId : message.getTo())
		{
			final MessageListener dest = programs.get(destId);
			if(dest!=null)
				dest.sendMessage(message);
		}

		if(message.getTo().isEmpty())
		{
			for(MessageListener dest : programs.values())
			{
				if(dest!=null)
					dest.sendMessage(message);
			}
		}
	}

	private synchronized void sendToVisualizations(final Invoke message)
	{
		for(final String destId : message.getTo())
		{
			final MessageListener dest = visualizations.get(destId);
			if(dest!=null)
				dest.sendMessage(message);
		}

		if(message.getTo().isEmpty())
		{
			for(MessageListener dest : visualizations.values())
			{
				if(dest!=null)
					dest.sendMessage(message);
			}
		}
	}

	public synchronized void newVisualizationMessage(final Invoke message) {
		switch (message.getType()) {
		case COMMAND:
			sendToPrograms(message);
			break;

		default:
			break;
		}
		//TODO process JSON content
		//ie: add to MessageBus, store, notify message bus, etc
	}

	public synchronized void newProgramMessage(final Invoke message) {
		//TODO process JSON content
		//ie: add to MessageBus, store, notify message bus, etc
		switch (message.getType()) {
		case DATA:
			sendToVisualizations(message);
			data.add(message);
			break;
		default:
			break;
		}
	}

	public List<Invoke> getData() {
		return data;
	}

}
