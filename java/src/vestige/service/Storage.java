//package vestige.service;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//
//import org.json.JSONObject;
//
//import vestige.old.CommandListener;
//import vestige.old.DataHistory;
//import vestige.old.DataMessage;
//import vestige.old.DataRequest;
//import vestige.old.IncomingMessage;
//import vestige.old.Session;
//import vestige.old.SocketClient;
//
//public class Storage {
//	private static Storage instance=null;
//
//	private final List<DataMessage> receivedData;
//	private final Map<Session,Integer> connectedClients;
//	private final Map<String,Integer> lastIndexes, lastIds;
//	private final Set<CommandListener> listeners;
//	private final List<DataHistory> flatHistory;
//
//	private final List<SocketClient> connectedHistory;
//
//	public static Storage Instance()
//	{
//		if(instance==null)
//			instance=new Storage();
//
//		return instance;
//	}
//
//	private Storage()
//	{
//		receivedData=new ArrayList<>();
//		connectedClients=new HashMap<>();
//		listeners=new HashSet<>();
//		lastIndexes=new HashMap<>();
//		lastIds=new HashMap<>();
//		connectedHistory=new ArrayList<>();
//		flatHistory=new ArrayList<>();
//	}
//
//	public synchronized void clear()
//	{
//		connectedClients.clear();
//		lastIds.clear();
//		lastIndexes.clear();
//		receivedData.clear();
//		connectedHistory.clear();
//		flatHistory.clear();
//	}
//
//	public synchronized void addListener(final CommandListener l)
//	{
//		listeners.add(l);
//	}
//
//	public void removeListener(final CommandListener l) {
//		listeners.remove(l);
//	}
//
//	public synchronized void connect(final String clientId)
//	{
//		Session ses=new Session(clientId);
//		if(connectedClients.containsKey(ses)){
//			for(Session s : connectedClients.keySet())
//				if(s.equals(ses)){
//					s.addConnection();
//					break;
//				}
//		}
//		else{
//			connectedClients.put(ses,connectedHistory.size());
//			connectedHistory.add(new SocketClient(clientId, true));
//			
//			lastIndexes.put(clientId, -1);
//			lastIds.put(clientId, -1);
//		}
//	}
//
//	public synchronized void disconnect(final String clientId)
//	{
//		Session ses=new Session(clientId);
//
//		connectedHistory.get(connectedClients.get(ses)).setActive(false);
//		connectedClients.remove(clientId);
//		//		lastIndexes.remove(clientId);
//		//		lastIds.remove(clientId);
//	}
//
//	public synchronized void newMessage(final IncomingMessage msg)
//	{
//		final String clientID=msg.getClientID();
//
//		final int currentId=lastIds.get(clientID);
//		final int id=msg.getId();
//		final int size=msg.size();
//
//		if(currentId<0 || id!=currentId) //not part of a multi part matrix
//		{
//			lastIndexes.put(clientID, receivedData.size());
//			lastIds.put(clientID, id);
//
//
//			final DataMessage data = new DataMessage(msg);
//			receivedData.add(data);
//
//			final DataHistory hist = new DataHistory(data);
//			hist.addSize(msg.getProcess(), size);
//
//			flatHistory.add(hist);
//		}
//		else //a part of this matrix already arrived
//		{
//			for (int i = flatHistory.size()-1; i >= 0; --i) {
//				final DataHistory tmp=flatHistory.get(i);
//
//				if(tmp.getClientID().equals(clientID) && tmp.getID()==id)
//				{
//					tmp.addSize(msg.getProcess(), size);
//					break;
//				}
//			}
//
//			for (int i = receivedData.size()-1; i >= 0; --i) {
//				final DataMessage tmp=receivedData.get(i);
//
//				if(tmp.getClientID().equals(clientID) && tmp.getId()==id)
//				{
//					tmp.getData().add(msg);
//					break;
//				}
//			}
//		}
//
//	}
//
//
//
//	public synchronized DataMessage getLast(final String clientID)
//	{
//		final Integer index=lastIndexes.get(clientID);
//		if(receivedData.size()<=0 || index==null || index<0) return null;
//
//		return receivedData.get(index);	
//	}
//
//	public synchronized void newCommand(final JSONObject command) {
//		for(final CommandListener cl : listeners)
//			cl.newCommandArrived(command);
//
//	}
//
//	public List<SocketClient> getConnected() {
//		return connectedHistory;
//	}
//
//	public List<DataHistory> getHistory() {
//		return flatHistory;
//	}
//
//	public DataMessage get(final DataRequest req) {
//		for(final DataMessage msg : receivedData)
//			if( msg.getClientID().equals(req.getClientID()) &&
//					msg.getMessageID().equals(req.getMessageID()) )
//				return msg;
//
//		return null;
//	}
//
//	public void updateMatrix(JSONObject command) {
//		//{"data":[{"cols":[9,10],"entries":[9,100],"rows":[10,10]}],"command":"edit","clientID":"3rabjdvsfg"}
//		
//	}
//
//
//}
