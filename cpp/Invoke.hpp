//
// Created by Patrick Zulian on 19/05/16.
//

#ifndef VESTIGE_CPP_ADAPTER_INVOKE_HPP
#define VESTIGE_CPP_ADAPTER_INVOKE_HPP

#include <string>
#include <vector>
#include "vestige_StringOutputBuilder.hpp"

namespace vestige {

    class Invoke {
    public:
        void toJSON(std::ostream &os);
        void clear();

        void makeOpen(const std::string &from);

        const std::string &getType() const {
            return type_;
        }

        void setType_(const std::string &type_) {
            Invoke::type_ = type_;
        }

        const std::string &getData() const {
            return data_;
        }

        void setData_(const std::string &data_) {
            Invoke::data_ = data_;
        }

        const std::string &getFrom() const {
            return from_;
        }

        void setFrom_(const std::string &from_) {
            Invoke::from_ = from_;
        }

        const std::vector<std::string> &getTo() const {
            return to_;
        }

        void setTo_(const std::vector<std::string> &to_) {
            Invoke::to_ = to_;
        }

        const std::string &getContext() const {
            return context_;
        }

        void setContext_(const std::string &context_) {
            Invoke::context_ = context_;
        }

        const std::string &getNode() const {
            return node_;
        }

        void setNode_(const std::string &node_) {
            Invoke::node_ = node_;
        }

        Invoke();

    private:
        std::string type_;
        std::string data_;
        std::string from_;
        std::vector<std::string> to_;
        std::string context_;
        std::string node_;
    };
}


#endif //VESTIGE_CPP_ADAPTER_INVOKE_HPP
