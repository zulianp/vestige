//
// Created by Patrick Zulian on 02/11/15.
//

#include "vestige_Node.hpp"


namespace vestige {
    TraversalOption Set::accept(Visitor &visitor) {
       return visitor.visit(*this);
    }

    TraversalOption Var::accept(Visitor &visitor)
    {
        return visitor.visit(*this);
    }

    TraversalOption Scope::accept(Visitor &visitor)
    {
        return visitor.visit(*this);
    }
}