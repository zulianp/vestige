//
// Created by Patrick Zulian on 02/11/15.
//

#include "vestige_StringOutputBuilder.hpp"
namespace vestige {
    JSON::JSON(std::ostream &stream)
    : empty_(true), stream_(stream) { }

    bool JSON::empty() {
        return empty_;
    }

    void JSON::start() {
        empty_ = false;
        stream_.clear();
        stream_ << "{ ";
    }

    void JSON::add(const std::string &key, const long value) {
        stream_ << "\n\t\"" << key << "\" : " << value << ",";
    }

    void JSON::add(const std::string &key, const double value) {
        stream_ << "\n\t\"" << key << "\" : " << value << ",";
    }

    void JSON::add(const std::string &key, const std::string &value) {
        stream_ << "\n\t\"" << key << "\" : \"" << value << "\",";
    }

    void JSON::addStringifiedObject(const std::string &key, const std::string &value)
    {
        stream_ << "\n\t\"" << key << "\" : " << value << ",";
    }

    void JSON::finish() {
        stream_.seekp(-1, stream_.cur);
        stream_ << "\n}\n";
    }

    template<typename T>
    void stringify(const T *array, const unsigned long size, std::ostream &os, const std::string &separator = ","){
        if(size <= 0) return;

        long sizem1 = size-1;
        for(long i = 0; i < sizem1; ++i) {
            os << array[i] << separator;
        }


        os << array[sizem1];
    }

    void JSON::add(const std::string &key, const double * values, const unsigned long size) {
        stream_ << "\n\t\"" << key << "\" : [";
        stringify(values, size, stream_, ",");
        stream_ << "],";
    }

    void JSON::add(const std::string &key, const long * values, const unsigned long size) {
        stream_ << "\n\t\"" << key << "\" : [";
        stringify(values, size, stream_, ",");
        stream_ << "],";
    }

    void JSON::add(const std::string &key, const int * values, const unsigned long size) {
        stream_ << "\n\t\"" << key << "\" : [";
        stringify(values, size, stream_, ",");
        stream_ << "],";
    }

    void JSON::add(const std::string &key, const std::string * values, const unsigned long size)
    {
        stream_ << "\n\t\"" << key << "\" : [";
        stringify(values, size, stream_, ",");
        stream_ << "],";
    }
}
