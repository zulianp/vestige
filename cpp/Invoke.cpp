//
// Created by Patrick Zulian on 19/05/16.
//

#include "Invoke.hpp"


namespace vestige {
    void Invoke::toJSON(std::ostream &os) {
        JSON json(os);
        json.start();
        json.add("type", type_);
        json.addStringifiedObject("data", data_);
        json.add("from", from_);
        json.add("to", to_);
        json.add("context", context_);
        json.add("node", node_);
        json.finish();
    }

    Invoke::Invoke() { }

    void Invoke::clear() {
        type_ = "";
        data_ = "";
        from_ = "";
        to_.clear();
        context_ = "";
        node_ = "";
    }

    void Invoke::makeOpen(const std::string &from) {
        clear();
        type_ = "OPEN";
        from_ = from;

        std::stringstream ss;

        JSON json(ss);
        ss << "\n";
        json.start();
        json.add("type", "void");
        json.finish();
        ss << "\n";

        data_ = ss.str();
    }
}