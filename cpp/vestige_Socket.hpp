//
// Created by Patrick Zulian on 02/11/15.
//

#ifndef VESTIGE_CPP_ADAPTER_VESTIGE_SOCKET_HPP
#define VESTIGE_CPP_ADAPTER_VESTIGE_SOCKET_HPP

#include <memory>
#include "vestige_Base.hpp"

namespace vestige {

    class Socket {
    public:
        class AbstractSocketImpl {
        public:
            virtual bool openSendClose(const byte * buff, const long size) = 0;
            virtual bool good() const = 0;
            virtual bool open() = 0;
            virtual bool write(const std::string &msg) = 0;
            virtual bool read(std::string &msg) = 0;
            virtual bool close() = 0;
            virtual ~AbstractSocketImpl() {}
        };

        Socket(const std::string &host, const std::string &port);
        bool openSendClose(const byte * buff, const long size);
        bool good() const;
        bool open();
        bool write(const std::string &msg);
        bool read(std::string &msg);
        bool close();

    private:
        std::shared_ptr<AbstractSocketImpl> implementation_;
    };

    bool send(const std::string &host, const std::string &port, const byte * buff, const long size);
};

#endif //VESTIGE_CPP_ADAPTER_VESTIGE_SOCKET_HPP
