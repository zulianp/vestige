//
// Created by Patrick Zulian on 02/11/15.
//

#include "vestige_Command.hpp"

#include "rapidjson/document.h"		// rapidjson's DOM-style API
#include "rapidjson/prettywriter.h"	// for stringify JSON
#include "rapidjson/filestream.h"	// wrapper of C stream for prettywriter as output

#include <iostream>


namespace vestige {

    static std::shared_ptr<Set> make_set_node(const rapidjson::Value &data)
    {
        std::shared_ptr<Set> edit = std::make_shared<Set>();
        for(auto it = data.Begin(); it != data.End(); ++it) {
            const rapidjson::Value &setObject = *it;

            assert(setObject.HasMember("rows"));
            assert(setObject.HasMember("cols"));
            assert(setObject.HasMember("entries"));

            const rapidjson::Value &rows = setObject["rows"];
            const rapidjson::Value &cols = setObject["cols"];
            const rapidjson::Value &entries = setObject["entries"];

            edit->resize(entries.Size());
            for(auto i = 0; i < entries.Size(); ++i) {
                edit->entries()[i] = entries[i].GetDouble();
                edit->rows()[i] = rows[i].GetInt();
                edit->cols()[i] = cols[i].GetInt();
            }
        }
        return edit;
    }

    static std::shared_ptr<Program> make_program_node(const rapidjson::Value &data)
    {
        using std::make_shared;

        std::shared_ptr<Program> program = std::make_shared<Program>();
        const std::string type = data["type"].GetString();
        if(type != "Program") {
            std::cerr << "Expected type : Program pair wrong format, returning empty program" << std::endl;
            return program;
        }

        auto const &body = data["body"];

        auto scope = make_shared<Scope>();
        for(rapidjson::SizeType mIt = 0; mIt != body.Size(); ++mIt) {
            auto const &mem = body[mIt];
            const std::string mem_type = mem["type"].GetString();
            if(mem_type == "VariableDeclaration") {
                auto const &decl = mem["declarations"];
                //TODO ... do stuff

            } else if(mem_type == "ExpressionStatement") {
                auto const &expr = mem["expression"];
               const std::string expr_type = expr["type"].GetString();
                auto const &callee = expr["callee"];
                const std::string callee_name = callee["name"].GetString();
                auto const &args = expr["arguments"];

                std::cout << callee_name << std::endl;
                std::cout << "================" << std::endl;
                const std::string arg_name = args[rapidjson::SizeType(0)]["name"].GetString();

                std::cout << arg_name << std::endl;
                std::cout << "================" << std::endl;

                assert(arg_name == "self");
                assert(callee_name == "norm2");
                auto arg_node  = make_shared<Self>();
                auto expr_node = make_shared<Norm2>(arg_node);
                scope->add(expr_node);
            }
        }

        program->setRoot(scope);
        return program;
    }

    static std::shared_ptr<Norm2> make_norm_node(const rapidjson::Value &data) {
        return std::make_shared<Norm2>();
    }

    bool Command::fromJSON(const std::string &jsonString) {

        std::string nonConstString = jsonString;
        rapidjson::Document json;
        if(json.ParseInsitu<0>(&nonConstString[0]).HasParseError())  {
            std::cerr << "Settings::fromJSON(...) : JSON Syntax error" << std::endl;
            std::cerr << json.GetParseError() << std::endl;
            return false;
        }

        if(json.HasMember("command")) {
           name_ = json["command"].GetString();
        }

        if(name_ == vestige::EDIT) {
            if(json.HasMember("data")) {
                const rapidjson::Value &data = json["data"];
                exprtree_ = make_set_node(data);
            } else {
                std::cerr << "[Vestige] needs memeber data " << std::endl;
            }
        }

        if(name_ == vestige::RUN) {
            if(json.HasMember("data")) {
                const rapidjson::Value &data = json["data"];
                exprtree_ = make_program_node(data);
            } else {
                std::cerr << "[Vestige] needs memeber data " << std::endl;
            }
        }

        return true;
    }
}