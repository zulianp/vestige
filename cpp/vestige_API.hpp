//
// Created by Patrick Zulian on 02/11/15.
//

#ifndef VESTIGE_CPP_ADAPTER_VESTIGE_API_CPP_HPP
#define VESTIGE_CPP_ADAPTER_VESTIGE_API_CPP_HPP

#include "vestige_Command.hpp"
#include "vestige_Socket.hpp"
#include "Invoke.hpp"

#include <string>
#include <assert.h>

namespace vestige {


    void abort() {
//        send("localhost", "8888", EXIT, 4);
        exit(0);
    }


    template<typename Scalar, typename Ordinal>
    class Matrix {
    public:
        Ordinal rows, cols;
        Ordinal localCols, localRows;
        Ordinal offsetRow, offsetCol;
        Ordinal localNEntries;
        int process;
        long id;

        std::string path;
        std::string type;

        Scalar *entries;
        Ordinal *colindex;
        Ordinal *rowptr;

        Matrix() {
            clear();
        }

        void makeDense(Scalar *entries, const Ordinal rows, const Ordinal cols) {
            clear();
            this->rows = rows;
            this->cols = cols;
            this->localRows = rows;
            this->localCols = cols;

            this->entries = entries;
        }

        Matrix(Scalar *entries, const Ordinal rows, const Ordinal cols) {
            makeDense(entries, rows, cols);
        }

        void clear() {
            rows = 0;
            cols = 0;
            localRows = 0;
            localCols = 0;
            localNEntries = 0;

            offsetRow = 0;
            offsetCol = 0;
            process = 0;

            id = -1;

            path = "matrix";
            type = "dense";

            entries = nullptr;
            colindex = nullptr;
            rowptr = nullptr;
        }

        bool serialize(OutputStream &builder) const {
            assert(entries && "entries must be set");
            if (!entries) return false;

            builder.start();

            builder.add("rows", rows);
            builder.add("cols", cols);

            builder.add("offsetRow", offsetRow);
            builder.add("offsetCol", offsetCol);
            builder.add("localRows", localRows);
            builder.add("localCols", localCols);

            builder.add("id", id);
            builder.add("name", path);
            builder.add("process", process);

            builder.add("type", type);

            if (type == "crs") {
                assert(colindex && rowptr);
                if (colindex && rowptr) {
                    builder.add("colindex", colindex, localNEntries);
                    builder.add("rowptr", rowptr, localRows + 1);
                    builder.add("entries", entries, localNEntries);
                } else {
                    return false;
                }
            } else if (type == "dense") {
                builder.add("entries", entries, localRows * localCols);
            } else {
                assert(false && "Format not supported");
                return false;
            }

            builder.finish();
            return true;
        }
    };

    template<typename Scalar, typename Ordinal, class Function>
    bool interact(const Matrix<Scalar, Ordinal> &matrix, Function fun) {
        Socket socket("localhost", "8888");


        if (!socket.write("sessionID:asdasd\n")) //FIXME with random id
            return false;

        std::stringstream ss;
        JSON json(ss);
        matrix.serialize(json);
        if (!socket.write(ss.str())) {
            return false;
        }

        while (socket.good()) {
            std::string msg;
            if (!socket.read(msg)) {
                return false;
            }

            if (msg == "ping" || msg.empty()) {
                continue;
            }

            Command cmd;
            cmd.fromJSON(msg);

            if (cmd.name() == vestige::EXIT) {
                socket.write(vestige::EXIT);
                std::cout << "[Vestige] Exiting..." << std::endl;
                vestige::abort();
            } else if (cmd.name() == vestige::CONTINUE) {
                std::cout << "[Vestige] Continuing..." << std::endl;
                return true;
            }

            std::stringstream ss;
            JSON json(ss);
            json.start();
            fun(cmd, json);
            json.finish();
            if (!json.empty()) {
                std::cout << " result: " << ss.str() << std::endl;
            }
        }
        socket.write(vestige::EXIT);
        return true;
    }

    bool open_connection() {
        Socket socket("localhost", "8888");

        std::stringstream ss;
        Invoke inv;
        inv.makeOpen("sessionID:asdasd");
        inv.toJSON(ss);

        if (!socket.write(ss.str())) {
            return false;
        }

        while (socket.good()) {
            std::string msg;
            if (!socket.read(msg)) {
                return false;
            }

            if (msg.empty()) {
                continue;
            }

            std::cout << msg << std::endl;
            break;
        }

        socket.close();
        return true;
    }
}

#endif //VESTIGE_CPP_ADAPTER_VESTIGE_API_CPP_HPP
