//
// Created by Patrick Zulian on 02/11/15.
//

#ifndef VESTIGE_CPP_ADAPTER_VESTIGE_STRINGOUTPUTBUILDER_HPP
#define VESTIGE_CPP_ADAPTER_VESTIGE_STRINGOUTPUTBUILDER_HPP


#include <string>
#include <sstream>
#include <iostream>
#include <vector>

namespace vestige {

    class OutputStream {
    public:
        virtual bool empty() = 0;
        virtual void start() {}
        virtual void finish() {}
        virtual void add(const std::string &key, const int value) { add(key, static_cast<long>(value)); }
        virtual void add(const std::string &key, const long value) = 0;
        virtual void add(const std::string &key, const double value) = 0;
        virtual void add(const std::string &key, const std::string &value) = 0;
        virtual void addStringifiedObject(const std::string &key, const std::string &value) = 0;

        virtual void add(const std::string &key, const double * values, const unsigned long size) = 0;
        virtual void add(const std::string &key, const long * values, const unsigned long size) = 0;
        virtual void add(const std::string &key, const int * values, const unsigned long size) = 0;
        virtual void add(const std::string &key, const std::string * values, const unsigned long size) = 0;
        virtual ~OutputStream() {}
    };

    class JSON : public OutputStream {
    public:
        bool empty() override;
        void start() override;
        void add(const std::string &key, const long value) override;
        void add(const std::string &key, const double value) override;
        void add(const std::string &key, const std::string &value) override;
        void addStringifiedObject(const std::string &key, const std::string &value) override ;


        void add(const std::string &key, const double * values, const unsigned long size) override;
        void add(const std::string &key, const long * values, const unsigned long size) override;
        void add(const std::string &key, const int * values, const unsigned long size) override;
        void add(const std::string &key, const std::string * values, const unsigned long size) override;

        template<typename T>
        void add(const std::string &key, std::vector<T> &values) {
            if(values.empty()) {
                add(key, (T *)NULL, 0);
            } else {
                add(key, &values[0], values.size());
            }
        }

        void finish() override;
        JSON(std::ostream &stream);
    private:
        bool empty_;
        std::ostream & stream_;
    };

//    class HTML : public OutputStream {
//    public:
//        void start() override
//        {
//            stream_.clear();
//            stream_ << "<!DOCTYPE html><html><body><ul>\n";
//        }
//
//        void add(const std::string &key, const int value) override
//        {
//            stream_ << "\t<li><b>" << key << "</b>:" << value << "</li>\n";
//        }
//
//        void add(const std::string &key, const std::string &value) override
//        {
//            stream_ << "\t<li><b>" << key << "</b>:" << value << "</li>\n";
//        }
//
//        void finish() override
//        {
//            stream_ << "</ul></body></html>";
//
//        }
//
//        std::string str() const
//        {
//            return stream_.str();
//        }
//
//    private:
//        std::stringstream stream_;
//    };
}

#endif //VESTIGE_CPP_ADAPTER_VESTIGE_STRINGOUTPUTBUILDER_HPP
