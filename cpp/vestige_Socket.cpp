//
// Created by Patrick Zulian on 02/11/15.
//

#include "vestige_Socket.hpp"
#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <iostream>

namespace vestige {

    class BoostSocketImpl : public Socket::AbstractSocketImpl {
    public:
        BoostSocketImpl(const std::string &host, const std::string &port)
                : host_(host), port_(port) { }

        ~BoostSocketImpl() {
            close();
        }


        bool openSendClose(const byte *buff, const long size) override {
            using boost::asio::ip::tcp;
            using boost::system::error_code;
            using boost::asio::write;

            try {
                boost::asio::io_service io_service;
                tcp::resolver resolver(io_service);
                tcp::resolver::query query(host_, port_);
                tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);

                tcp::socket socket(io_service);
                boost::asio::connect(socket, endpoint_iterator);
                boost::system::error_code error;

                write(socket, boost::asio::buffer(buff, size), boost::asio::transfer_all(), error);

            } catch (std::exception &e) {
                std::cerr << e.what() << std::endl;
                return false;
            }

            return true;
        }

        bool good() const override {
            return bool(socket_) && socket_->is_open();
        }

        bool open() override {
            using boost::asio::ip::tcp;
            try {

                tcp::resolver resolver(service_);
                tcp::resolver::query query(host_, port_);
                tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);

                socket_ = std::make_shared<BoostSocket>(service_);
                boost::asio::connect(*socket_, endpoint_iterator);
                socket_->set_option(tcp::no_delay(true));
                return true;

            } catch (std::exception &e) {
                socket_ = nullptr;
                std::cerr << e.what() << std::endl;
                return false;
            }
        }

        bool write(const std::string &msg) override {
            try {
                if (!socket_) {
                    if (!open()) {
                        return false;
                    }
                }

                std::string realMsg = std::to_string(msg.size()) + "\n";
                realMsg += msg;
                boost::system::error_code error;
                boost::asio::write(*socket_, boost::asio::buffer(realMsg.c_str(), realMsg.size()),
                                   boost::asio::transfer_all(), error);

//                socket_->send(boost::asio::buffer(msg.c_str(), msg.size()));
//                socket_->c
                return true;
            } catch (std::exception &e) {
                std::cerr << e.what() << std::endl;
                return false;
            }
        }

        bool read(std::string &msg) override {
            try {
                if (!socket_) {
                    if (!open()) {
                        return false;
                    }
                }

                long size = -1;
                long total_bytes = 0;

                std::stringstream ss;

                boost::asio::streambuf buff;
                std::istream is(&buff);
                while (true) {
                    boost::asio::read_until(*socket_, buff, '\n');

                    std::string line;
                    std::getline(is, line);

                    if (size < 0) {
                        size = std::atol(line.c_str());
                    } else {
                        total_bytes += line.size() + 1;
                        ss << line;

//                    std::cout << "----------------------------------------\n";
//                    std::cout << line << std::endl;
//                    std::cout << total_bytes  << " " << size  << std::endl;
//                    std::cout << "----------------------------------------\n";

                        if (total_bytes >= size) {
                            break;
                        }
                    }
                }

                msg = ss.str();
                return true;
            } catch(const std::exception &e) {
                std::cerr << e.what() << std::endl;
                return false;
            }
        }

        bool close() override {
            if (!socket_) return true;
            socket_->close();
            socket_ = nullptr;
            return true;
        }

    private:
        typedef boost::asio::ip::tcp::socket BoostSocket;
        const std::string host_, port_;

        boost::asio::io_service service_;
        std::shared_ptr<BoostSocket> socket_;
    };

    Socket::Socket(const std::string &host, const std::string &port)
            : implementation_(std::make_shared<BoostSocketImpl>(host, port)) { }

    bool Socket::openSendClose(const byte *buff, const long size) {
        return implementation_->openSendClose(buff, size);
    }

    bool Socket::good() const {
        return implementation_->good();
    }

    bool Socket::open() {
        return implementation_->open();
    }

    bool Socket::write(const std::string &msg) {
        return implementation_->write(msg);
    }

    bool Socket::read(std::string &msg) {
        return implementation_->read(msg);
    }

    bool Socket::close() {
        return implementation_->close();
    }

    bool send(const std::string &host, const std::string &port, const byte *buff, const long size) {
        Socket socket(host, port);
        return socket.write(std::string(reinterpret_cast<char const *>(buff), size));
    }
}
