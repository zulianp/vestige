//
// Created by Patrick Zulian on 02/11/15.
//

#ifndef VESTIGE_CPP_ADAPTER_VESTIGE_NODE_HPP_HPP
#define VESTIGE_CPP_ADAPTER_VESTIGE_NODE_HPP_HPP

#include <iostream>
#include <vector>
#include <map>

namespace vestige {

    enum TraversalOption {
        CONTINUE_TRAVERSAL = 0,
        STOP_TRAVERSAL = 1
    };

    class Visitor;
    class Program;
    class Norm2;
    class Self;
    class Scope;

    class Node {
    public:
        virtual TraversalOption accept(Visitor &visitor) = 0;

        virtual ~Node() { }
    };

    class Var : public Node {
    public:
        TraversalOption accept(Visitor &visitor);

        inline const std::string &name() const {
            return name_;
        }

        inline void set_name(const std::string &name) {
            name_ = name;
        }

    private:
        std::string name_;
    };

    class Scope : public Node {
    public:
        typedef std::vector<std::shared_ptr<Node> > NodeVectorT;

        TraversalOption accept(Visitor &visitor);

        inline void add(const std::shared_ptr<Node> &node) {
            nodes_.push_back(node);
        }

        inline const NodeVectorT &nodes() const {
            return nodes_;
        }

        inline NodeVectorT &nodes() {
            return nodes_;
        }

        inline void set_parent_scope(const std::shared_ptr<Node> &parent) {
            parent_ = parent;
        }

        inline std::shared_ptr<Node> get_parent_scope() {
            return parent_;
        }

        void add(const std::string &name, const std::shared_ptr<Node> &symbol)
        {
            symbols_[name] = symbol;
        }

        std::shared_ptr<Node> symbol(const std::string &name) const
        {
            auto it = symbols_.find(name);
            if(it == symbols_.end()) return nullptr;
            return it->second;
        }

    private:

        std::map<std::string, std::shared_ptr<Node> > symbols_;
        std::shared_ptr<Node> parent_;
        NodeVectorT nodes_;
    };

    class Set : public Node {
    public:
        TraversalOption accept(Visitor &visitor) override;

        inline const std::vector<double> &entries() const {
            return entries_;
        }

        inline const std::vector<long> &rows() const {
            return rows_;
        }

        inline const std::vector<long> &cols() const {
            return cols_;
        }

        inline std::vector<double> &entries() {
            return entries_;
        }

        inline std::vector<long> &rows() {
            return rows_;
        }

        inline std::vector<long> &cols() {
            return cols_;
        }

        inline void resize(const unsigned long size) {
            entries_.resize(size);
            rows_.resize(size);
            cols_.resize(size);
        }

    private:
        std::vector<double> entries_;
        std::vector<long> rows_;
        std::vector<long> cols_;
    };


    class Unary : public Node {
    public:

        const std::shared_ptr<Node> &node()
        {
            return node_;
        }

        Unary(const std::shared_ptr<Node> &node = nullptr)
                : node_(node)
        {}

    private:
        std::shared_ptr<Node> node_;
    };

    template<typename T>
    class Value : public Node {
    public:
        inline const T &get() const
        {
            return value_;
        }
    private:
        T value_;
    };


    class Visitor {
    public:
        virtual TraversalOption visit(Set &) {
            std::cout << "Visiting Set" << std::endl;
            return CONTINUE_TRAVERSAL;
        }

        virtual TraversalOption visit(Var &) {
            std::cout << "Visiting Var" << std::endl;
            return CONTINUE_TRAVERSAL;
        }

        virtual TraversalOption visit(Scope &) {
            std::cout << "Visiting Scope" << std::endl;
            return CONTINUE_TRAVERSAL;
        }

        virtual TraversalOption visit(Value<double> &) {
            std::cout << "Visiting Value<double> " << std::endl;
            return CONTINUE_TRAVERSAL;
        }

        virtual TraversalOption visit(Value<long> &) {
            std::cout << "Visiting Value<long> " << std::endl;
            return CONTINUE_TRAVERSAL;
        }

        virtual TraversalOption visit(Program &) {
            std::cout << "Visiting Program " << std::endl;
            return CONTINUE_TRAVERSAL;
        }

        virtual TraversalOption visit(Norm2 &) {
            std::cout << "Visiting Norm2 " << std::endl;
            return CONTINUE_TRAVERSAL;
        }

        virtual TraversalOption visit(Self &) {
            std::cout << "Visiting Self " << std::endl;
            return CONTINUE_TRAVERSAL;
        }
    };

    class Norm2 : public Unary {
    public:
        using Unary::Unary;

       inline TraversalOption accept(Visitor &visitor) override
        {
            return visitor.visit(*this);
        }
    };

    class Program : public Node {
    public:
        const std::shared_ptr<Node> &root() {
            return root_;
        }

       inline void setRoot(const std::shared_ptr<Node> &root)
        {
            root_ = root;
        }

        inline TraversalOption accept(Visitor &visitor) override
        {
            return visitor.visit(*this);
        }
    private:
        std::shared_ptr<Node> root_;
    };



    class Self : public Node {
    public:
        TraversalOption accept(Visitor &visitor) override
        {
           return visitor.visit(*this);
        }
    };
}



#endif //VESTIGE_CPP_ADAPTER_VESTIGE_NODE_HPP_HPP
