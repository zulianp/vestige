//
// Created by Patrick Zulian on 02/11/15.
//

#ifndef VESTIGE_CPP_ADAPTER_VESTIGE_JSONOBJECT_HPP
#define VESTIGE_CPP_ADAPTER_VESTIGE_JSONOBJECT_HPP

#include <string>
#include "vestige_Node.hpp"



namespace vestige {
    static const char *EXIT = "exit";
    static const char *CONTINUE = "continue";
    static const char *EDIT = "edit";
    static const char *RUN = "run";

    class Command {
    public:
         inline const std::string &name() const
         {
             return name_;
         }

        inline std::shared_ptr<Node> exprtree()
        {
            return exprtree_;
        }

        bool fromJSON(const std::string &jsonString);
    private:

        std::shared_ptr<Node> exprtree_;
        std::string name_;
    };
}

#endif //VESTIGE_CPP_ADAPTER_VESTIGE_JSONOBJECT_HPP
