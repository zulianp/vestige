#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

#include "vestige_StringOutputBuilder.hpp"
#include "vestige_API.hpp"


static const long ROWS = 20;
static const long COLS = 20;

class ExampleVisitor : public vestige::Visitor {
public:
    ExampleVisitor(std::vector<double> &data, vestige::OutputStream &os)
            : data_(data), os_(os)
    {}

    virtual vestige::TraversalOption visit(vestige::Program &program) override
    {
        return program.root()->accept(*this);
    }

    virtual vestige::TraversalOption visit(vestige::Scope &scope) override
    {

        for(auto n : scope.nodes()) {
            if(vestige::STOP_TRAVERSAL == n->accept(*this)) {
                return vestige::STOP_TRAVERSAL;
            }
        }
        return vestige::CONTINUE_TRAVERSAL;
    }

    virtual vestige::TraversalOption visit(vestige::Norm2 &norm2) override {
        double result = 0;
        for(auto v : data_) {
            result += v*v;
        }

        os_.add("norm2", std::sqrt(result));
        return vestige::CONTINUE_TRAVERSAL;
    }

    virtual vestige::TraversalOption visit(vestige::Set &setnode) override
    {
        for(unsigned long k = 0; k < setnode.entries().size(); ++k) {
            long i = setnode.rows()[k];
            long j = setnode.cols()[k];
            data_[i*COLS + j] = setnode.entries()[k];
            std::cout << i << ", " << j << "-> " << setnode.entries()[k] << std::endl;
        }

        return vestige::CONTINUE_TRAVERSAL;
    }

    std::vector<double> &data_;
    vestige::OutputStream &os_;
};

int main() {
    using namespace vestige;

//    std::vector<double> dense_mat(ROWS*COLS, 0);
//    for(int i = 0; i < ROWS; ++i) {
//        for(int j = 0; j < COLS; ++j) {
//            dense_mat[i*COLS + j] = i*j * (double(std::rand())/RAND_MAX > 0.4);
//        }
//    }
//
//    // dense_mat[ROWS/2*COLS + COLS/2]=1000;
//
//    Matrix<double, int> mat;
//    mat.makeDense(&dense_mat[0], ROWS, COLS);
//
//    interact(mat, [&dense_mat](Command &cmd, OutputStream &os) {
//        std::cout << cmd.name() << std::endl;
//        if(cmd.exprtree()) {
//            ExampleVisitor v(dense_mat, os);
//            cmd.exprtree()->accept(v);
//        }
//    });

    open_connection();

    return 0;
}

